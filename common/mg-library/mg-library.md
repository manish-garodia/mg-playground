# MG Library

## Topic Title
- [Access VM and Configure VNC](https://manish-garodia.github.io/mg-playground/topic-title/access-vm-config-vnc/)
- [Oracle Database 21c Gold Image](https://manish-garodia.github.io/mg-playground/topic-title/install-db-goldimage/)
- [Install Oracle EMCC 13.5](https://manish-garodia.github.io/mg-playground/topic-title/install-emcc/)

![Image Alt text](./../images/maintenance-work-in-progress.jpg " ")**Caption:** Work-in-progress

