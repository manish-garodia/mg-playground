# Welcome to MG space


[![](./../../../learning-library/common/images/livelabs-banner-formarketplace.png)](http://bit.ly/golivelabs)
----

- 👋 Hi, I’m <mark>@manish-garodia</mark>
<i>

----
- 👀 &nbsp;&nbsp;I’m exploring &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ins>GitHub, GitBash, GitDesktop</ins>.  
- 🌱 &nbsp;&nbsp;I’m from &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="background-color: #FCF3CF">Oracle Database UA development</span> team.
- 💞️ &nbsp;&nbsp;I’m looking &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to collaborate on developing workshops on LiveLabs.
</i>

----
- 📫 Reach me at [manish.garodia@oracle.com](./files/email.md)

## DBA Essentials workshops

> These workshops are developed from the [2 Day DBA Book](https://docs.oracle.com/en/database/oracle/oracle-database/19/admqs/index.html) with reference to the [learning path](https://apexapps.oracle.com/pls/apex/f?p=44785:50:1090764338649:::50:P50_COURSE_ID,P50_EVENT_ID:458,6362).

----

Select a version to view the workshop. 
<style>
.heatMap {
    width: 105%;
    text-align: left;
}
.heatMap th {
background: light grey;
word-wrap: break-word;
text-align: center;
}
.heatMap tr:nth-child(1) { background: white; }
.heatMap tr:nth-child(2) { background: #FEF5E7; }
.heatMap tr:nth-child(3) { background: #EAFAF1; }
.heatMap tr:nth-child(4) { background: #EAFAF1; }
.heatMap tr:nth-child(5) { background: #EAFAF1; }
.heatMap tr:nth-child(6) { background: #FDEDEC; }
.heatMap tr:nth-child(7) { background: #FDEDEC; }
.heatMap tr:nth-child(8) { background: #EBF5FB; }
.heatMap tr:nth-child(9) { background: #EBF5FB; }
.heatMap tr:nth-child(10) { background: #EBF5FB; }
.heatMap tr:nth-child(11) { background: #EBF5FB; }
.heatMap tr:nth-child(12) { background: #FEF5E7; }
</style>

<div class="heatMap">

| WS <br> Gr-No. </br>|Title                            | WMS ID | Clone        | Fork | learning <br>library</br> |
|:----------------:|---------------------------------|:------:|:------------:|:----:|:-------------------------:|
|**0**             | DBA Essentials workshops series |        | [Freetier](http://127.0.0.1:3001/mg-playground/projects/dba-essentials-test/workshops/freetier/) | [Freetier](https://manish-garodia.github.io/mg-playground/projects/dba-essentials-test/workshops/freetier/) | |
|**A-1**             | [Install Oracle Database 21c on OCI Compute](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?wid=871) | [7041](https://apex.oraclecorp.com/pls/apex/f?p=24885:14:116446876260617::::P14_ID:7041) | [Freetier](http://127.0.0.1:3001/mg-playground/projects/dba-essentials-test/install-db/workshops/freetier/?lab=dbca-typical-advanced#Task3:CreateandConfigureaContainerDatabase(AdvancedMode)) | [Freetier](https://manish-garodia.github.io/learning-library/data-management-library/database/21c/dba-essentials/install-db/workshops/freetier/) | [Freetier](https://oracle.github.io/learning-library/data-management-library/database/21c/dba-essentials/install-db/workshops/freetier/) | 
|**B-2**             | [DB Admin with Oracle Enterprise <br>Manager Cloud Control</br>](https://apexapps.oracle.com/pls/apex/dbpm/r/livelabs/view-workshop?wid=918) | [7141](https://apex.oraclecorp.com/pls/apex/f?p=24885:320:12978619964771::::P320_ROWID:ADaLzyAIsAAACxCAAB) |  [Freetier](http://127.0.0.1:3001/mg-playground/projects/dba-essentials-test/em-dba/workshops/freetier/)  | [Freetier](https://manish-garodia.github.io/learning-library/data-management-library/database/21c/dba-essentials/em-dba/workshops/freetier/) | [Freetier](https://oracle.github.io/learning-library/data-management-library/database/21c/dba-essentials/em-dba/workshops/freetier/) |
|**B-3**             | Configure Network Environment | [8321](https://apex.oraclecorp.com/pls/apex/f?p=24885:320:13437580376201::::P320_ROWID:ADaLzyAIsAAAAVhAAA) | [Freetier](http://127.0.0.1:3001/mg-playground/projects/dba-essentials-test/configure-network-env/workshops/freetier/) | [Freetier](https://manish-garodia.github.io/learning-library/data-management-library/database/21c/dba-essentials/configure-network-env/workshops/freetier/) | [Freetier](https://oracle.github.io/learning-library/data-management-library/database/21c/dba-essentials/configure-network-env/workshops/freetier/) |
|**B-4**             | Manage Database Instance and Memory | [9341](https://apex.oraclecorp.com/pls/apex/f?p=24885:14:107660186661678::::P14_ID:9341) | [Freetier](http://127.0.0.1:3001/mg-playground/projects/dba-essentials-test/manage-instance-memory/workshops/freetier/) |||
|**C-5**             | TBD                             |        |            |      |                           |
|**C-6**             | TBD                             |        |            |      |                           |
|**D-7**             | TBD                             |        |            |      |                           |
|**D-8**             | Backup and Recovery Operations  | [9281](https://apex.oraclecorp.com/pls/apex/f?p=24885:14:12214020181921::::P14_ID:9281) | [Freetier](http://127.0.0.1:3001/mg-playground/projects/dba-essentials-test/backup-recovery/workshops/freetier/) |||
|**D-9**             | TBD                             |        |            |      |                           |
|**D-10**            | TBD                             |        |            |      |                           |
|**A-11**            | Uninstall Oracle Database From Your Host | [8341](https://apex.oraclecorp.com/pls/apex/f?p=24885:320:12904126839438::::P320_ROWID:ADaLzyAIsAAAAXJAAA) | Freetier | Freetier | Freetier |

</div class="heatMap">

## **Playground** - Test Env


|-----------------+------------+-----------------+----------------|
| Default aligned |Left aligned| Center aligned  | Right aligned  |
|-----------------|:-----------|:---------------:|---------------:|
| First body part |Second cell | Third cell      | fourth cell    |
| Second line     |foo         | **strong**      | baz            |
| Third line      |quux        | baz             | bar            |
|-----------------+------------+-----------------+----------------|
| Second body     |            |                 |                |
| 2 line          |            |                 |                |
+=================|============|=================|================+
| Footer row      |            |                 |                |
|-----------------+------------+-----------------+----------------|

## Test Tables

|---
| Default aligned | Left aligned | Center aligned | Right aligned
|-|:-|:-:|-:
| First body part | Second cell | Third cell | fourth cell
| Second line |foo | **strong** | baz
| Third line |quux | baz | bar
|---
| Second body
| 2 line
|===
| Footer row

## Acknowledgements

 - **Author** - Manish Garodia, Team Database UAD
 - **Last Updated on** - December 23, (Thu) 2021

<!---
manish-garodia/manish-garodia is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
